﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Together.Activity.API.Models
{
    public class ActivityViewModel
    {
        public ActivityViewModel()
        {
            Participants = new List<ParticipantViewModel>();
        }

        public int ActivityId { get; set; }
        public string OwnerId { get; set; }
        public string Description { get; set; }
        public string Details { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime ActivityStartTime { get; set; }
        public DateTime ActivityEndTime { get; set; }
        public DateTime EndRegisterTime { get; set; }
        public string Address { get; set; }
        public int? LimitsNum { get; set; }
        public string Status { get; set; }

        public List<ParticipantViewModel> Participants { get; set; }
    }

    public class ParticipantViewModel
    {
        public string UserId { get; set; }
        public string Nickname { get; set; }
        public DateTime JoinTime { get; set; }
        public string Avatar { get; set; }
        public int Sex { get; set; }
    }
}
